<?xml version='1.0'?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version="1.0">
                
    <xsl:param name="category"/>            
    <xsl:param name="subcategory"/>
                
    <xsl:template match="/">
    	<html>
    		<head>
    			<title>Goods</title>
    			<style type="text/css">
					.button 
					{
						background-color: #b0bca7;
					}
					td
					{
						width: 100px;
					}
				</style>
    		</head>
  			<body>
  				<a href="/xsltransformations/index?command=categories">
    				Categories
    			</a>
    			>>
    			<a href="/xsltransformations/index?command=subcategories&amp;category={$category}">
    				<xsl:value-of select="$category"/>
    			</a>
    			>> <xsl:value-of select="$subcategory"/>
    			<br/>
    			<br/>
	  			<table>
	  				<tr style="background-color: #b0bca7;">
						<td>Producer</td>
						<td>Model</td>
						<td>Date of issue</td>
						<td>Color</td>
						<td>Price</td>
					</tr>
	  				<xsl:for-each select="Products/Category[@name=$category]/Subcategory[@name=$subcategory]">
			    		<xsl:apply-templates/>
			    	</xsl:for-each>
				</table>
				<form action="index" method="post">
					<input type="hidden" name="category" value="{$category}"/>
					<input type="hidden" name="subcategory" value="{$subcategory}"/>
					<input type="submit" style="font-size: 18px" 
							name="command" value="add" class="button"/>
				</form>
  			</body>
  		</html>
    </xsl:template>
    
    <xsl:template match="Good">
		<tr>
			<td><xsl:value-of select="Producer"/></td>
			<td><xsl:value-of select="Model"/></td>
			<td><xsl:value-of select="Date-of-issue"/></td>
			<td><xsl:value-of select="Color"/></td>
			<xsl:choose>
				<xsl:when test="Not-in-stock">
					<td>Not in stock</td>
				</xsl:when>
				<xsl:otherwise>
					<td><xsl:value-of select="Price"/></td>
				</xsl:otherwise>
			</xsl:choose>
		</tr>
	</xsl:template>

</xsl:stylesheet>