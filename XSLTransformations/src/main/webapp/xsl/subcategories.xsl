<?xml version='1.0'?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version="1.0">
                
    <xsl:param name="category"/>
    
    <xsl:template match="/">
    	<html>
    		<head>
    			<title>Subcategories</title>
    		</head>
  			<body>
  				<a href="/xsltransformations/index?command=categories">
    				Categories
    			</a>
    			>> <xsl:value-of select="$category"/>
    			<br/>
    			<br/>
  				<table>
			    	<xsl:for-each select="Products/Category[@name=$category]">
			    		<xsl:apply-templates/>
			    	</xsl:for-each>
    			</table>
  			</body>
  		</html>
    </xsl:template>
                
    <xsl:template match="Subcategory">
    	<xsl:variable name="name">
	  		<xsl:value-of select="@name"/>
	  	</xsl:variable>
		<tr>
			<td width="100px">
				<a href="/xsltransformations/index?command=goods&amp;category={$category}&amp;subcategory={$name}">
    				<xsl:value-of select="$name"/>
    			</a>
			</td>
			<td><xsl:value-of select="count(Good)"/></td>
		</tr>
	</xsl:template>

</xsl:stylesheet>