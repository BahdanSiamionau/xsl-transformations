<?xml version='1.0'?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:validator="xalan://com.epam.xsltransformations.validation.InputValidator"
				xmlns:result="xalan://com.epam.xsltransformations.validation.ValidationResult"
				xmlns:good="xalan://com.epam.xsltransformations.model.Good"
            	version="1.0">
    
    <xsl:import href="add.xsl"/>
    
    <xsl:param name="category"/>            
    <xsl:param name="subcategory"/>
    <xsl:param name="good"/>
    <xsl:param name="result"/>
    
    <xsl:template match="/">
    	<xsl:choose>
    		<xsl:when test="validator:validate($result, $good)">
    			<xsl:apply-templates/>
    		</xsl:when>
    		<xsl:otherwise>
    			<xsl:call-template name="error-form"/>
    		</xsl:otherwise>
    	</xsl:choose>
    </xsl:template>
    
    <xsl:template match="@* | node()">
    	<xsl:copy>
       		<xsl:apply-templates select="@* | node()"/>
    	</xsl:copy>
	</xsl:template>
	
	<xsl:template match="Products/Category[@name=$category]/Subcategory[@name=$subcategory]">
   		<xsl:copy>
        	<xsl:apply-templates select="@* | node()"/>
       		<xsl:element name="Good">
      			<xsl:element name="Producer">
					<xsl:value-of select="good:getProducer($good)"/>
				</xsl:element>
				<xsl:element name="Model">
					<xsl:value-of select="good:getModel($good)"/>
				</xsl:element>
				<xsl:element name="Date-of-issue">
					<xsl:value-of select="good:getDateOfIssue($good)"/>
				</xsl:element>
				<xsl:element name="Color">
					<xsl:value-of select="good:getColor($good)"/>
				</xsl:element>
				<xsl:choose>
					<xsl:when test="good:getPrice($good)">
						<xsl:element name="Price">
							<xsl:value-of select="good:getPrice($good)"/>
						</xsl:element>
					</xsl:when>
					<xsl:otherwise>
						<xsl:element name="Not-in-stock"/>
					</xsl:otherwise>
				</xsl:choose>
       		</xsl:element>
    	</xsl:copy>
	</xsl:template>

</xsl:stylesheet>