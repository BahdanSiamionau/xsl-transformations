<?xml version='1.0'?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version="1.0">
                
    <xsl:template match="/">
    	<html>
    		<head>
    			<title>Categories</title>
    		</head>
  			<body>
  				Categories
  				<br/>
  				<br/>
	  			<table>
	  				<xsl:for-each select="Products/Category"> 
	  					<xsl:variable name="name">
	  						<xsl:value-of select="@name"/>
	  					</xsl:variable>
						<tr>
    						<td width="100px">
    							<a href="/xsltransformations/index?command=subcategories&amp;category={$name}">
    								<xsl:value-of select="$name"/>
    							</a>
    						</td>
    						<td>
    							<xsl:value-of select="count(Subcategory/Good)"/>
    						</td>
    					</tr>
					</xsl:for-each>
				</table>
  			</body>
  		</html>
    </xsl:template>

</xsl:stylesheet>