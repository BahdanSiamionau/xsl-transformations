<?xml version='1.0'?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:result="xalan://com.epam.xsltransformations.validation.ValidationResult"
				xmlns:good="xalan://com.epam.xsltransformations.model.Good"
				xmlns:map="xalan://java.util.HashMap"
				xmlns:list="xalan://java.util.ArrayList"
                version="1.0">
                
    <xsl:param name="category"/>            
    <xsl:param name="subcategory"/>
    <xsl:param name="good"/>
    <xsl:param name="result"/>
    
    <xsl:variable name="resultExist">
    	<xsl:value-of select="$result"/>
    </xsl:variable>
    
    <xsl:variable name="goodExist">
    	<xsl:value-of select="$good"/>
    </xsl:variable>
    
    <xsl:variable name="map" select="result:getErrorMap($result)"/>
    
    <xsl:template match="/">
    	<xsl:call-template name="error-form"/>
    </xsl:template>
                
    <xsl:template name="error-form">
    	<html>
    		<head>
    			<title>Adding new good</title>
    			<script type="text/javascript">
				    function checkBoxToggled(){
				    	checkbox = document.form.notInStock;
				        if(checkbox.checked) {
				            document.form.price.disabled=true;
				            document.form.price.value="";
				            document.form.price.style.backgroundColor="#b0bca7";
				        }
				        else {
				            document.form.price.disabled=false;
				            document.form.price.style.backgroundColor="white";
				        }
				    }
				</script>
				<style type="text/css">
					.button 
					{
						background-color: #b0bca7;
					}
				</style>
    		</head>
  			<body onLoad="checkBoxToggled()">
  				<a href="/xsltransformations/index?command=categories">
    				Categories
    			</a>
    			>>
    			<a href="/xsltransformations/index?command=subcategories&amp;category={$category}">
    				<xsl:value-of select="$category"/>
    			</a>
    			>> 
    			<a href="/xsltransformations/index?command=goods&amp;category={$category}&amp;subcategory={$subcategory}">
    				<xsl:value-of select="$subcategory"/>
    			</a>
    			>> Add
    			<br/>
    			<br/>
    			<form name="form" action="index" method="post">
		  			<table>
		  				<tr>
							<td width="100px">Producer</td>
							<td>
								<xsl:choose>
									<xsl:when test="$goodExist">
										<xsl:variable name="content">
											<xsl:value-of select="good:getProducer($good)"/>
										</xsl:variable>
										<input type="text" name="producer" value="{$content}"/>
									</xsl:when>
									<xsl:otherwise>
										<input type="text" name="producer"/>
									</xsl:otherwise>
								</xsl:choose>
							</td>
						</tr>
						<xsl:if test="$resultExist">
							<xsl:if test="map:containsKey($map, 'producer')">
								<xsl:variable name="list" select="map:get($map, 'producer')"/>
								<xsl:variable name="length" select="list:size($list)"/>
							
								<xsl:if test="$length > 0">
									<tr>
										<td></td>
										<td style="color: red;">
											<ul>
												<xsl:call-template name="looper">
												    <xsl:with-param name="iterations" select="$length - 1"/>
												    <xsl:with-param name="list" select="$list"/>
												</xsl:call-template>
											</ul>
										</td>
									</tr>
								</xsl:if>
							</xsl:if>
						</xsl:if>
						<tr>
							<td width="100px">Model</td>
							<td>
								<xsl:choose>
									<xsl:when test="$goodExist">
										<xsl:variable name="content">
											<xsl:value-of select="good:getModel($good)"/>
										</xsl:variable>
										<input type="text" name="model" value="{$content}"/>
									</xsl:when>
									<xsl:otherwise>
										<input type="text" name="model"/>
									</xsl:otherwise>
								</xsl:choose>
							</td>
						</tr>
						<xsl:if test="$resultExist">
							<xsl:if test="map:containsKey($map, 'model')">
								<xsl:variable name="list" select="map:get($map, 'model')"/>
								<xsl:variable name="length" select="list:size($list)"/>
								
								<xsl:if test="$length > 0">
									<tr>
										<td></td>
										<td style="color: red;">
											<ul>
												<xsl:call-template name="looper">
												    <xsl:with-param name="iterations" select="$length - 1"/>
												    <xsl:with-param name="list" select="$list"/>
												</xsl:call-template>
											</ul>
										</td>
									</tr>
								</xsl:if>
							</xsl:if>
						</xsl:if>
						<tr>
							<td width="100px">Date of issue</td>
							<td>
								<xsl:choose>
									<xsl:when test="$goodExist">
										<xsl:variable name="content">
											<xsl:value-of select="good:getDateOfIssue($good)"/>
										</xsl:variable>
										<input type="text" name="date" value="{$content}"/>
									</xsl:when>
									<xsl:otherwise>
										<input type="text" name="date"/>
									</xsl:otherwise>
								</xsl:choose>
							</td>
						</tr>
						<xsl:if test="$resultExist">
							<xsl:if test="map:containsKey($map, 'date')">
								<xsl:variable name="list" select="map:get($map, 'date')"/>
								<xsl:variable name="length" select="list:size($list)"/>
								
								<xsl:if test="$length > 0">
									<tr>
										<td></td>
										<td style="color: red;">
											<ul>
												<xsl:call-template name="looper">
												    <xsl:with-param name="iterations" select="$length - 1"/>
												    <xsl:with-param name="list" select="$list"/>
												</xsl:call-template>
											</ul>
										</td>
									</tr>
								</xsl:if>
							</xsl:if>
						</xsl:if>
						<tr>
							<td width="100px">Color</td>
							<td>
								<xsl:choose>
									<xsl:when test="$goodExist">
										<xsl:variable name="content">
											<xsl:value-of select="good:getColor($good)"/>
										</xsl:variable>
										<input type="text" name="color" value="{$content}"/>
									</xsl:when>
									<xsl:otherwise>
										<input type="text" name="color"/>
									</xsl:otherwise>
								</xsl:choose>
							</td>
						</tr>
						<xsl:if test="$resultExist">
							<xsl:if test="map:containsKey($map, 'color')">
								<xsl:variable name="list" select="map:get($map, 'color')"/>
								<xsl:variable name="length" select="list:size($list)"/>
								
								<xsl:if test="$length > 0">
									<tr>
										<td></td>
										<td style="color: red;">
											<ul>
												<xsl:call-template name="looper">
												    <xsl:with-param name="iterations" select="$length - 1"/>
												    <xsl:with-param name="list" select="$list"/>
												</xsl:call-template>
											</ul>
										</td>
									</tr>
								</xsl:if>
							</xsl:if>
						</xsl:if>
						<tr>
							<td width="100px">Price</td>
							<td>
								<xsl:choose>
									<xsl:when test="$goodExist">
										<xsl:variable name="content">
											<xsl:value-of select="good:getPrice($good)"/>
										</xsl:variable>
										<input type="text" name="price" value="{$content}"/>
									</xsl:when>
									<xsl:otherwise>
										<input type="text" name="price"/>
									</xsl:otherwise>
								</xsl:choose>
							</td>
						</tr>
						<tr>
							<td>
								<xsl:choose>
									<xsl:when test="$goodExist">
										<xsl:choose>
											<xsl:when test="good:isNotInStock($good)">
												<input name="notInStock" type="checkbox" 
												onClick="checkBoxToggled()" checked="checked"/>
											</xsl:when>
											<xsl:otherwise>
												<input name="notInStock" type="checkbox" 
												onClick="checkBoxToggled()"/>
											</xsl:otherwise>
										</xsl:choose>
									</xsl:when>
									<xsl:otherwise>
										<input name="notInStock" type="checkbox" 
										onClick="checkBoxToggled()"/>
									</xsl:otherwise>
								</xsl:choose>
								Good is not in stock
							</td>
						</tr>
						<xsl:if test="$resultExist">
							<xsl:if test="map:containsKey($map, 'price')">
								<xsl:variable name="list" select="map:get($map, 'price')"/>
								<xsl:variable name="length" select="list:size($list)"/>
								
								<xsl:if test="$length > 0">
									<tr>
										<td></td>
										<td style="color: red;">
											<ul>
												<xsl:call-template name="looper">
												    <xsl:with-param name="iterations" select="$length - 1"/>
												    <xsl:with-param name="list" select="$list"/>
												</xsl:call-template>
											</ul>
										</td>
									</tr>
								</xsl:if>
							</xsl:if>
						</xsl:if>
					</table>
					<input type="hidden" name="command" value="save"/>
					<input type="hidden" name="category" value="{$category}"/>
					<input type="hidden" name="subcategory" value="{$subcategory}"/>
					<input type="submit" style="font-size: 18px; float: left; margin-top: 19px;" 
							name="command" value="save" class="button"/>
				</form>
				<form action="index" method="post">
					<input type="hidden" name="command" value="goods"/>
					<input type="hidden" name="category" value="{$category}"/>
					<input type="hidden" name="subcategory" value="{$subcategory}"/>
					<input type="submit" style="font-size: 18px; margin-left: 50px;" 
							value="cancel" class="button"/>
				</form>
  			</body>
  		</html>
    </xsl:template>
    
    <xsl:template name="looper">
	    <xsl:param name="iterations"/>
	    <xsl:param name="list"/>
	    <xsl:if test="$iterations > -1">
	        <li>
	        	<xsl:value-of select="list:get($list, $iterations)"></xsl:value-of>
	        </li>
	        <xsl:call-template name="looper">
	            <xsl:with-param name="iterations" select="$iterations - 1"/>
	            <xsl:with-param name="list" select="$list"/>
	         </xsl:call-template>
	    </xsl:if>
	</xsl:template>

</xsl:stylesheet>