package com.epam.xsltransformations.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.epam.xsltransformations.command.ICommand;
import com.epam.xsltransformations.exception.TransformationException;
import com.epam.xsltransformations.table.CommandTable;

@SuppressWarnings("serial")
@WebServlet("/TransformationServlet")
public final class TransformationServlet extends HttpServlet {
	
	private static final Logger LOG = Logger.getLogger(TransformationServlet.class);
       
	public TransformationServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		performTask(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		performTask(request, response);
	}
	
	private void performTask(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		CommandTable requestHelper = CommandTable.getInstance();
		ICommand command = requestHelper.getCommand(request);
		try {
			command.execute(request, response);
		} catch (TransformationException ex) {
			LOG.error(ex.getMessage());
		}
	}
}
