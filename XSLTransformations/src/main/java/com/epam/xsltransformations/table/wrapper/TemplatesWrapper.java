package com.epam.xsltransformations.table.wrapper;

import javax.xml.transform.Templates;

public final class TemplatesWrapper {
	
	private Templates templates;
	private long modified;
	
	public TemplatesWrapper() {
		
	}
	
	public TemplatesWrapper(Templates templates, long modified) {
		this.templates = templates;
		this.modified = modified;
	}
	
	public Templates getTemplates() {
		return templates;
	}
	public void setTemplates(Templates templates) {
		this.templates = templates;
	}
	public long getModified() {
		return modified;
	}
	public void setModified(long modified) {
		this.modified = modified;
	}
}
