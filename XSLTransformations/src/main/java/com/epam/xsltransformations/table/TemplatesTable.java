package com.epam.xsltransformations.table;

import java.io.File;
import java.util.HashMap;

import javax.xml.transform.Templates;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamSource;

import org.apache.log4j.Logger;

import com.epam.xsltransformations.exception.TransformationException;
import com.epam.xsltransformations.resources.TransformationBundle;
import com.epam.xsltransformations.table.wrapper.TemplatesWrapper;

public final class TemplatesTable {
	
	private HashMap <String, TemplatesWrapper> templatesMap = new HashMap<String, TemplatesWrapper> ();
	private static final TransformerFactory FACTORY = TransformerFactory.newInstance();
	private static final Logger LOG = Logger.getLogger(TemplatesTable.class);
	
	private static final class InstanceHolder {
		private static final TemplatesTable instance = new TemplatesTable(); 
	}
	
	public static TemplatesTable getInstance() {
		return InstanceHolder.instance;
	}
	
	private TemplatesTable() {
		
	}
	
	public synchronized Templates getTemplates(String pathName) throws TransformationException {
		TemplatesWrapper wrapper;
		try {
			String path = TransformationBundle.getString(pathName);
			if (templatesMap.containsKey(pathName)) {
				wrapper = templatesMap.get(pathName);
			} else {
				wrapper = new TemplatesWrapper(FACTORY.newTemplates(new StreamSource(TransformationBundle.getString(pathName))), 
						new File(TransformationBundle.getString(pathName)).lastModified());
				templatesMap.put(pathName, wrapper);
			}
			long lastModified = new File(path).lastModified();
			if (lastModified > wrapper.getModified()) {
				wrapper.setTemplates(FACTORY.newTemplates(new StreamSource(path)));
				wrapper.setModified(lastModified);
			}
		} catch (TransformerConfigurationException ex) {
			LOG.error(ex.getMessage());
			throw new TransformationException(ex);
		}
		return wrapper.getTemplates();
	}
}

