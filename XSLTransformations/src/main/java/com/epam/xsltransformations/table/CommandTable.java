package com.epam.xsltransformations.table;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import com.epam.xsltransformations.command.AddCommand;
import com.epam.xsltransformations.command.CategoriesCommand;
import com.epam.xsltransformations.command.GoodsCommand;
import com.epam.xsltransformations.command.ICommand;
import com.epam.xsltransformations.command.SaveCommand;
import com.epam.xsltransformations.command.SubcategoriesCommand;
import com.epam.xsltransformations.resources.TransformationConstants;

public final class CommandTable {

	private HashMap <String, ICommand> commands = new HashMap <String, ICommand> ();
	
	private static final class InstanceHolder {
		private static final CommandTable instance = new CommandTable(); 
	}
	
	public static CommandTable getInstance() {
		return InstanceHolder.instance;
	}
	
	private CommandTable() {
		commands.put(TransformationConstants.CATEGORIES, new CategoriesCommand());
		commands.put(TransformationConstants.SUBCATEGORIES, new SubcategoriesCommand());
		commands.put(TransformationConstants.GOODS, new GoodsCommand());
		commands.put(TransformationConstants.ADD, new AddCommand());
		commands.put(TransformationConstants.SAVE, new SaveCommand());
	}
	
	public ICommand getCommand(HttpServletRequest request) {
		String commandName = request.getParameter(TransformationConstants.COMMAND);
		ICommand command = commands.get(commandName);
		if (command == null) {
			command = new CategoriesCommand();
		}
		return command;
	}
}

