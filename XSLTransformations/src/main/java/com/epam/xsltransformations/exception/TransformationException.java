package com.epam.xsltransformations.exception;

@SuppressWarnings("serial")
public final class TransformationException extends Exception {
	
	public TransformationException() {}
	
	public TransformationException(String msg) {
		super(msg);
	}
	
	public TransformationException(Exception ex) {
		super(ex);
	}
}
