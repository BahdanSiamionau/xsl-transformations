package com.epam.xsltransformations.command;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.transform.Templates;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.log4j.Logger;

import com.epam.xsltransformations.exception.TransformationException;
import com.epam.xsltransformations.resources.TransformationBundle;
import com.epam.xsltransformations.resources.TransformationConstants;
import com.epam.xsltransformations.table.TemplatesTable;

public final class AddCommand implements ICommand {

	private static final Logger LOG = Logger.getLogger(AddCommand.class);

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws TransformationException {
		try {  
			Templates templates = TemplatesTable.getInstance().getTemplates(TransformationConstants.XSL_ADD);
			Transformer transformer = templates.newTransformer();
			transformer.setParameter(TransformationConstants.CATEGORY, request.getParameter(TransformationConstants.CATEGORY));
			transformer.setParameter(TransformationConstants.SUBCATEGORY, request.getParameter(TransformationConstants.SUBCATEGORY));
			READ_WRITE_LOCK.readLock().lock();
			transformer.transform(new StreamSource(TransformationBundle.getString(TransformationConstants.XML_PATH)), new StreamResult(response.getWriter()));
		} catch (TransformerException | IOException | TransformationException ex) {
			LOG.error(ex.getMessage());
			throw new TransformationException(ex);
		} finally {
			READ_WRITE_LOCK.readLock().unlock();
		}
	}
}
