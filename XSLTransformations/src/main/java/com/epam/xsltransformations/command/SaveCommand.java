package com.epam.xsltransformations.command;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Formatter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.transform.Templates;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.log4j.Logger;

import com.epam.xsltransformations.exception.TransformationException;
import com.epam.xsltransformations.model.Good;
import com.epam.xsltransformations.resources.TransformationBundle;
import com.epam.xsltransformations.resources.TransformationConstants;
import com.epam.xsltransformations.table.TemplatesTable;
import com.epam.xsltransformations.validation.ValidationResult;

public final class SaveCommand implements ICommand {
	
	private static final Logger LOG = Logger.getLogger(SaveCommand.class);
	private static final String PATH = "/xsltransformations/index?command=goods&category=%s&subcategory=%s";

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws TransformationException {
		Writer stringWriter = null, fileWriter = null, bufferedWriter = null;
		Good good;
		ValidationResult result;
		Templates templates;
		Transformer transformer;
		try {
			good = new Good();
			good.setProducer(request.getParameter(TransformationConstants.PRODUCER));
			good.setModel(request.getParameter(TransformationConstants.MODEL));
			good.setDateOfIssue(request.getParameter(TransformationConstants.DATE));
			good.setColor(request.getParameter(TransformationConstants.COLOR));
			good.setPrice(request.getParameter(TransformationConstants.PRICE));
			good.setNotInStock(TransformationConstants.ON
					.equals(request.getParameter(TransformationConstants.NOT_IN_STOCK)) ? true : false);
			
			stringWriter = new StringWriter();
			result = new ValidationResult();
			
			templates = TemplatesTable.getInstance().getTemplates(TransformationConstants.XSL_SAVE);
			transformer = templates.newTransformer();
			transformer.setParameter(TransformationConstants.CATEGORY, request.getParameter(TransformationConstants.CATEGORY));
			transformer.setParameter(TransformationConstants.SUBCATEGORY, request.getParameter(TransformationConstants.SUBCATEGORY));
			transformer.setParameter(TransformationConstants.GOOD, good);
			transformer.setParameter(TransformationConstants.RESULT, result);
			
			READ_WRITE_LOCK.readLock().lock();
			try {
				transformer.transform(new StreamSource(TransformationBundle.getString(TransformationConstants.XML_PATH)), new StreamResult(stringWriter));
			} finally {
				READ_WRITE_LOCK.readLock().unlock();
			}
			
			if (result.isLastValidationSuccessful()) {
				READ_WRITE_LOCK.writeLock().lock();
				try {
					fileWriter = new FileWriter(TransformationBundle.getString(TransformationConstants.XML_PATH));
					bufferedWriter = new BufferedWriter(fileWriter);
					bufferedWriter.write(stringWriter.toString());
				} finally {
					READ_WRITE_LOCK.writeLock().unlock();
				}
				response.sendRedirect(redirectToURL(request.getParameter(TransformationConstants.CATEGORY), request.getParameter(TransformationConstants.SUBCATEGORY)));
			} else {
				response.getWriter().write(stringWriter.toString());
			}
		} catch (TransformerException | TransformationException | IOException ex) {
			LOG.error(ex.getMessage());
			throw new TransformationException(ex);
		} finally {
			quietWriterClose(stringWriter);
			quietWriterClose(bufferedWriter);
			quietWriterClose(fileWriter);
		}
	}
	
	private void quietWriterClose(Writer writer) {
		try {
			if (writer != null) {
				writer.close();
			}
		} catch (IOException ex) {
			LOG.info(ex.getMessage());
		}
	}
	
	@SuppressWarnings("resource")
	private String redirectToURL(String category, String subcategory) {
		return new Formatter().format(PATH, category, subcategory).toString();
	}
}
