package com.epam.xsltransformations.command;

import java.util.concurrent.locks.ReentrantReadWriteLock;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.epam.xsltransformations.exception.TransformationException;

public interface ICommand {
	
	static final ReentrantReadWriteLock READ_WRITE_LOCK = new ReentrantReadWriteLock();
	
	void execute(HttpServletRequest request, HttpServletResponse response) throws TransformationException;
}	