package com.epam.xsltransformations.validation;

import java.util.ArrayList;
import java.util.HashMap;

public final class ValidationResult {

	private HashMap<String, ArrayList<String>> errorMap = new HashMap<>();
	private boolean lastValidationSuccessful;

	public boolean isLastValidationSuccessful() {
		return lastValidationSuccessful;
	}

	public void setLastValidationSuccessful(boolean lastValidationSuccessful) {
		this.lastValidationSuccessful = lastValidationSuccessful;
	}

	public HashMap<String, ArrayList<String>> getErrorMap() {
		return errorMap;
	}

	public void setErrorMap(HashMap<String, ArrayList<String>> errorMap) {
		this.errorMap = errorMap;
	}
}
