package com.epam.xsltransformations.validation;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.epam.xsltransformations.model.Good;
import com.epam.xsltransformations.resources.TransformationConstants;
import com.epam.xsltransformations.resources.Color;

public final class InputValidator {
	
	private static final String MODEL_REGEX = "[a-zA-Z]{2}[0-9]{3}";
	private static final String PRICE_REGEX = "[1-9]{1}[0-9]*";
	private static final String DATE_REGEX 
		= "(0[1-9]|[12][0-9]|3[01])-(0[1-9]|1[012])-(197[1-9]|19[89][0-9]|200[0-9]|201[012])";
	private static final String ERROR_EMPTY = "Field should not be empty";
	private static final String ERROR_REGEX_MODEL = "Model name should consist of 2 letters and 3 digits.";
	private static final String ERROR_REGEX_DATE = "Date should match dd-MM-YYYY format.";
	private static final String ERROR_REGEX_PRICE = "Price should contain digits only.";
	private static final String ERROR_COLOR = "Color not exists. Color real names accepted only.";
	
	private InputValidator() {
		
	}
	
	public static boolean validate(ValidationResult result, Good good) {
		
		boolean validated = true;
		
		validated &= notEmpty(result, TransformationConstants.PRODUCER, good.getProducer());
		validated &= notEmpty(result, TransformationConstants.MODEL, good.getModel());
		validated &= notEmpty(result, TransformationConstants.DATE, good.getDateOfIssue());
		validated &= notEmpty(result, TransformationConstants.COLOR, good.getColor());
		if (good.getPrice() != null && !good.isNotInStock()) {
			validated &= notEmpty(result, TransformationConstants.PRICE, good.getPrice());
		}
		
		validated &= matchesPattern(result, TransformationConstants.MODEL, 
				MODEL_REGEX, good.getModel());
		validated &= matchesPattern(result, TransformationConstants.DATE, 
				DATE_REGEX, good.getDateOfIssue());
		if (good.getPrice() != null && !good.isNotInStock()) {
			validated &= matchesPattern(result, TransformationConstants.PRICE, 
					PRICE_REGEX, good.getPrice());
		}
		
		validated &= isColor(result, good.getColor());
		
		result.setLastValidationSuccessful(validated);
		return result.isLastValidationSuccessful();
	}

	private static boolean notEmpty(ValidationResult result, String name, String content) {
		if (content.isEmpty()) {
			addErrorToMap(result, ERROR_EMPTY, name);
			return false;
		}
		return true;
	}
	
	private static boolean matchesPattern(ValidationResult result, String name, String regex, String content) {
		
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(content);
		
		if (matcher.matches()) {
			return true;
		} 
		switch (name) {
		case TransformationConstants.MODEL:
			addErrorToMap(result, ERROR_REGEX_MODEL, name);
			break;
		case TransformationConstants.DATE:
			addErrorToMap(result, ERROR_REGEX_DATE, name);
			break;
		case TransformationConstants.PRICE:
			addErrorToMap(result, ERROR_REGEX_PRICE, name);
			break;
		}
		return false;
	}
	
	private static boolean isColor(ValidationResult result, String colorToFind) {
		for (Color color : Color.values()) {
			if (color.toString().equalsIgnoreCase(colorToFind)) {
				return true;
			}
		}
		addErrorToMap(result, ERROR_COLOR, TransformationConstants.COLOR);
		return false;
	}
	
	private static void addErrorToMap(ValidationResult result, String error, String fieldName) {
		ArrayList<String> list;
		if (result.getErrorMap().containsKey(fieldName)) {
			list = result.getErrorMap().get(fieldName);
			list.add(error);
		} else {
			list = new ArrayList<String>();
			list.add(error);
			result.getErrorMap().put(fieldName, list);
		}
	}
}
