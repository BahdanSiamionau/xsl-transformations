package com.epam.xsltransformations.resources;

public final class TransformationConstants {
	

	public static final String PATH = "path";
	public static final String COMMAND = "command";
	public static final String INDEX = "index";
	public static final String INDEX_PATH = "jsp.index.path";
	public static final String CATEGORIES = "categories";
	public static final String SUBCATEGORIES = "subcategories";
	public static final String CATEGORY = "category";
	public static final String SUBCATEGORY = "subcategory";
	public static final String GOOD = "good";
	public static final String GOODS = "goods";
	public static final String ADD = "add";
	public static final String SAVE = "save";
	public static final String XML_PATH = "xml.path";
	public static final String XSL_CATEGORIES = "xsl.categories.path";
	public static final String XSL_SUBCATEGORIES = "xsl.subcategories.path";
	public static final String XSL_GOODS = "xsl.goods.path";
	public static final String XSL_ADD = "xsl.add.path";
	public static final String XSL_SAVE = "xsl.save.path";
	public static final String PRODUCER = "producer";
	public static final String MODEL = "model";
	public static final String DATE = "date";
	public static final String COLOR = "color";
	public static final String PRICE = "price";
	public static final String NOT_IN_STOCK = "notInStock";
	public static final String ON = "on";
	public static final String RESULT = "result";
	
	private TransformationConstants() {
		
	}
}
