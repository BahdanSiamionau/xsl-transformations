package com.epam.xsltransformations.resources;

import java.util.ResourceBundle;

public final class TransformationBundle {

	private static final ResourceBundle BUNDLE = ResourceBundle.getBundle(TransformationConstants.PATH);
	
	private TransformationBundle() {
		
	}
	
	public static String getString(String key) {
		return BUNDLE.getString(key);
	}
}